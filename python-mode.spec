%global _empty_manifest_terminate_build 0
Name:		python-mode
Version:	4.4.0
Release:	1
Summary:	AsyncIO Service-based programming
License:	BSD and CC-BY-SA-4.0
URL:		https://github.com/ask/mode
Source0:	https://files.pythonhosted.org/packages/c2/37/0a34981928dd632ce6daba89f342fd4822f3649965cb83bc949de7f28b78/mode-4.4.0.tar.gz
BuildArch:	noarch

BuildRequires:	python3-colorlog
BuildRequires:	python3-mypy-extensions

%description
AsyncIO Service-based programming.

%package -n python3-mode
Summary:	AsyncIO Service-based programming.
Provides:	python-mode
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-mode
AsyncIO Service-based programming.

%package help
Summary:	Development documents and examples for mode
Provides:	python3-mode-doc
%description help
Development documents and examples for mode.

%prep
%autosetup -n mode-4.4.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-mode -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Jul 22 2021 wutao <wutao61@huawei.com> - 4.4.0-1
- Package init
